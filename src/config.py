import re
from pydantic import BaseSettings
import mattermostdriver


class Settings(BaseSettings):
    debug = True
    # Mattermost
    mm_url: str
    mm_verify: bool = True
    mm_token: str
    mm_channel_id: str

    def get_mmd(self):
        scheme, host, port = self._parse_url(self.mm_url)
        option = {
            'url': host,
            'scheme': scheme,
            'port': port,
            'token': self.mm_token,
            'verify': self.mm_verify
        }
        print(option)

        return mattermostdriver.Driver(option)

    @staticmethod
    def _parse_url(url: str):
        match = re.match(r'([^:]+)://(.+)', url)
        if match:
            scheme = match.group(1)
            host = match.group(2)

            if "/" in host:
                host = host.split("/")[0]

            if ":" in host:
                host, port = host.split(":")
            else:
                port = "443" if scheme == "https" else "80"

            return scheme, host, int(port)
        else:
            raise Exception(f"Invalid url ({url})")


settings = Settings()
