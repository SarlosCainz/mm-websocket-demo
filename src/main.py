import json
import urllib3

from config import settings
import util

logger = util.get_logger()
urllib3.disable_warnings()


async def event_handler(message):
    event = json.loads(message)
    if "event" in event:
        logger.debug(event["event"])
        if event["event"] == "posted":
            post_data = json.loads(event["data"]["post"])
            logger.debug(post_data)


def main():
    login_info = None
    try:
        mmd = settings.get_mmd()
        login_info = mmd.login()

        logger.debug(login_info)

        mmd.init_websocket(event_handler)
    except KeyboardInterrupt:
        pass
    finally:
        if login_info is not None:
            mmd.disconnect()


if __name__ == '__main__':
    main()
